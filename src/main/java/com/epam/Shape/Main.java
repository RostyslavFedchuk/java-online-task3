package com.epam.Shape;

import java.util.Scanner;
import java.util.SortedMap;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Type a shape to calculate a square of it: ");
        String shape = scan.nextLine();
        if(!shape.matches("[a-zA-Z]+")) {
            System.out.println("Wrong input!");
            return;
        }
        shape= shape.toLowerCase();
        System.out.print("Write the color of the " + shape + ": ");
        String color = scan.next();
        Shape one = null;
        if(shape.equals("circle")){
            one = new Circle();
            one.setColor(color);
            System.out.print("Write the radius of the "+color+" circle: ");
            Double R = scan.nextDouble();
            ((Circle) one).setRadius(R);
        }
        else if(shape.equals("rectangle")){
            one = new Rectangle();
            one.setColor(color);
            System.out.print("Write the width and heigth of the "+color+" rectangle: ");
            Double width = scan.nextDouble();
            Double height = scan.nextDouble();
            ((Rectangle) one).setParametrs(width, height);
        }
        else if(shape.equals("triangle")){
            one = new Triangle();
            one.setColor(color);
            System.out.print("Write lengths of 3 sides of the "+color+" triagle: ");
            Double a = scan.nextDouble();
            Double b = scan.nextDouble();
            Double c = scan.nextDouble();
            ((Triangle) one).setParametrs(a, b, c);
        }
        else{
            System.out.println("Sorry, but I don`t know how to calculate the square of " + shape);
            return;
        }
        System.out.println("The square of " + color + " " + shape + " = " + one.countSquare()+
                "\nI`m so smart and loyal. Appreciate me))");
    }
}
