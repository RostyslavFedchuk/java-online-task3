package com.epam.Shape;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public void setParametrs(double width, double height){
        if(width<=0 || height <=0) {
            System.out.println("Wrong input!");
            return;
        }
        this.width = width;
        this.height = height;
    }
    @Override
    public double countSquare() {
        return width*height;
    }
}