package com.epam.Shape;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public void setParametrs(double a, double b, double c){
        if(a<= 0 || b <= 0 || c<= 0) {
            System.out.println("Wrong input!");
            return;
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public double countSquare() {
        double p = (a+b+c)/2;
        return Math.sqrt(p*(p - a)*(p - b)*(p - c));
    }
}