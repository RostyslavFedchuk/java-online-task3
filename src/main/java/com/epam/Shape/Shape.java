package com.epam.Shape;

public abstract class Shape {
    private String color = null;

    public void setColor(String color){
        if(color==""){
            System.out.println("Wrong input!");
            return;
        }
        this.color = color;
    }
    public String getColor(){
        return color;
    }
    abstract public double countSquare();
}



