package com.epam.Shape;

public class Circle extends Shape {
    private double radius;

    public void setRadius(double radius){
        if(radius <= 0) {
            System.out.println("Wrong input!");
            return;
        }else{
            this.radius = radius;
        }
    }

    public double getRadius(){
        return radius;
    }
    @Override
    public double countSquare() {
        return Math.PI*Math.pow(radius,2);
    }
}