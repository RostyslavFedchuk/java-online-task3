package com.epam.Animal;

public class Bird extends Animal{
    public Bird(int E){
        super(E);
        System.out.println("I am a Bird!");
    }
    public void fly(){
        System.out.println("I`m flying!");
        if(energy >= 10) energy -= 10;
    }
}