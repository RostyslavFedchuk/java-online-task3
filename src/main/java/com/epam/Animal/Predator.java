package com.epam.Animal;

public class Predator extends Animal{
    public Predator(int E){
        super(E);
        System.out.println("I am a Predator!");
    }
    public void hunt(){
        System.out.println("I`m hunting!");
        if(energy >= 20) energy -= 20;
    }
}
