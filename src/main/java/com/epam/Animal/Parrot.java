package com.epam.Animal;

public class Parrot extends Bird{
    public Parrot(int E){
        super(E);
        System.out.println("I am a Parrot");
    }
    @Override
    public void eat(){
        System.out.println("I ate plants!");
        energy += 30;
    }
}