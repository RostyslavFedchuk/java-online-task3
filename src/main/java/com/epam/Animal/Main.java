package com.epam.Animal;

import com.epam.Animal.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        boolean goHome = false;
        boolean noAnimal = false;
        System.out.println("Hi! I'm Tommy. Today I`m going to be your guide in our Zoo.\n");
        while(!goHome){
            System.out.println("Which animal do you want to see?");
            String desire = scan.next();
            desire = desire.toLowerCase();
            Animal George = null;
            if(desire.equals("bird") || desire.equals("birds")){
                System.out.println("What kind of bird do you prefer?");
                desire = scan.next();
                desire = desire.toLowerCase();
                if(desire.equals("parrot")){
                    George = new Parrot(40);
                    George.eat();
                }else if(desire.equals("eagle")){
                    George = new Eagle(40);
                    George.work();
                }
            }
            else if(desire.equals("predator") || desire.equals("predators")){
                System.out.println("What kind of predators do you like?");
                desire = scan.next();
                desire = desire.toLowerCase();
                if(desire.equals("lion")){
                    George = new Lion(40);
                    ((Lion) George).hunt();
                }else if(desire.equals("tiger")){
                    George = new Tiger(40);
                    George.sleep();
                }
            }
            else if(desire.equals("fish") || desire.equals("fishes")){
                George = new Fish(40);
                ((Fish) George).swim();
            }
            else{
                noAnimal = true;
                System.out.println("Sorry, but we don`t have " + desire +
                        " or you haven`t typed the class of the animal");
            }
            if(!noAnimal){
                System.out.println("Do you want to see more?");
            }
            else{
                System.out.println("Type corectly or maybe you want to see someone else?\n" +
                        "We have Predators, fishes and Birds");
            }
            boolean isNoYes = false;
            while(!isNoYes){
                desire = scan.nextLine();
                desire = desire.toLowerCase();
                desire.trim();
                String yes = "[a-zA-Z,)!.\\s]*?yes[a-zA-Z,)!.\\s]*?";
                String no = "[a-zA-Z,)!.\\s]*?no[a-zA-Z,)!.\\s]*?";
                if(desire.matches(no)) {
                    isNoYes = true;
                    goHome = true;
                }
                else if(desire.matches(yes)){
                    isNoYes = true;
                }else{
                    System.out.print("Write yes or no: ");
                }
            }
        }

    }
}
