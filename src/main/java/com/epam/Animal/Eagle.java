package com.epam.Animal;

public class Eagle extends Bird{
    public Eagle(int E){
        super(E);
        System.out.println("I am an Eagle!");
    }
    @Override
    public void eat(){
        System.out.println("I ate meat!");
        energy += 30;
    }
}