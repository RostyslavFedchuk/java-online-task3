package com.epam.Animal;

public class Animal {
    protected int energy;
    public Animal(int E){
        energy = E;
    }
    public Animal(){}

    public void sleep(){
        System.out.println("I have slept!");
        energy += 100;
    }
    public void work(){
        System.out.println("I have worked!");
        if(energy >= 20) energy -= 20;
    }
    public void eat(){
        System.out.println("I eat anything!");
        energy += 25;
    }
}


