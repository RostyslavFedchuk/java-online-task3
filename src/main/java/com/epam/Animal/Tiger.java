package com.epam.Animal;

public class Tiger extends Predator {
    public Tiger(int E){
        super(E);
        System.out.println("I am a Tiger!");
    }

    @Override
    public void hunt(){
        System.out.println("I`m hunting antelopes");
        if(energy >= 25) energy -=25;
    }
}
