package com.epam.Animal;

public class Fish extends Animal{
    public Fish(int E){
        super(E);
        System.out.println("I am a Fish");
    }
    public void swim(){
        System.out.println("I'm swimming!");
        if(energy >= 10) energy -= 10;
    }
}